# README #

### Share File Package ###
The Share File is a standalone public NPM package for integrating Madison Logic services with ShareFile account. It will upload files to share file account, creates folder, crates share links for files and provides restrective access.

## How do I get set up for development? ##
Development requirements:
Node.js 4 LTS
share file account

Do not commit any production credentials into this git repository!

## Install Dependencies: ##
npm install

## How to run tests ##
npm test
Deployment instructions
To be determined.

## Contribution guidelines
Writing tests
Use Mocha.js and place tests in the the "test" directory.

## Code review ##
All changes must be submitted as pull requests and reviewed by a fellow human being, then merged to master only when the review is approved and tests are all passing.

## Other guidelines ##
Who do I talk to?
Repository and project owners
* Pavan Deshmukh <pavande@cybage.com>
* Arun Wadikar <arunw@cybage.com>

Other team contacts
To be determined